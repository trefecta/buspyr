#!/usr/bin/env python

"""
    Class for BusPirate serial communication.

    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>

"""
import serial
import select
import time
import sys
from pathlib import Path

import binascii

import constants
import exceptions

class SerialConnector(object):
    def __init__(self, port=None, baud=constants.BAUD, timeout=0.05, is_verbose=False):
        """
        Connect to a Serial Device
            FIRMWARE 5.10
            BOOTLOADER 4.4

        Parameters
        ==========
            port - (str):
                Optional. Device descriptor path (com port not tested).
                Default: see `constants.py`
            baud - (int):
                Optional. Serial baud rate.
                Default: see `constants.py`
            timeout - (float):
                Optional. Milliseconds to wait during timeout.
                Default: 0.05
            is_verbose - (bool)
                Optional. Stream output to `stdout`
                Default: False
                
        """

        # Internal setup
        self._is_verbose = is_verbose

        self.__port = port or self.find_device_port()
        self.__baud = baud
        self.__timeout_duration = timeout        

        # print("SerialConnector :::", dir(self))


    ###
    ##  Class Context Manager
    ###

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()


    ###
    ## Device Access and Control Methods
    ###

    def connect(self, port=None, baud=None, timeout=None):
        """
          Connect to bus pirate
        """
        self.__port = port or self.__port
        self.__baud = baud or self.__baud
        self.__timeout_duration = timeout or self.__timeout_duration

        self.__stdout("Connecting to {} at {}Hz...\n\n", self.__port, self.__baud)

        self.__connection = serial.Serial(self.__port, self.__baud, timeout=self.__timeout_duration)

        return self.__connection

    def close(self):
        """
            Close serial connection.

            Call this before exiting.
        """
        if self.__connection.isOpen():
            self.reset()
            self.__connection.close()

    def find_device_port(self, substrs=["tty", "usb"]):
        """
        Find the device port 
        
        Parameters
        ==========
            substrs - (list(str) | str):
                Optional. substrings to be found in device pathname.
                Default: ["tty", "usb"] 
        """
        if isinstance(substrs, str):
            substrs = [substrs]

        device_paths = Path("/dev")

        for device_path in device_paths.iterdir():
            if device_path.is_dir():
                continue

            if all([(substr in device_path.name.lower()) for substr in substrs]):
                self.__stdout("Device matching {} found: {}\n", ', '.join(substrs), str(device_path))
                return str(device_path)

        raise OSError("Cannot find device matching terms: {:s}.\n".format(', '.join(substrs)))

    def read(self, length=0, has_cache=False, to_hex=False):
        """
        Read from serial port

        Setting `has_cache` to True, the response is cached, 
            retrievable through the `get_cache` method.

        Parameters
        ==========
            length - (int):
                Optional. Number of bytes to read.
                Default: 0
            has_cache - (bool)
                Optional. Whether or not to cache response.
                Default: False
        """
        try:
            if length:
                res = self.__connection.read(length)
            elif self.__timeout_duration:
                # Must have timeout enabled for "readlines"
                res = self.__connection.readlines()
        except Exception as e:
            self.__stderr(e)
            raise exceptions.SerialReadError(length)

        tmp = res[:]


        if to_hex:
            if not isinstance(tmp, int) and len(tmp):
                if not isinstance(tmp[0], int):
                    return([hex(c) for b in tmp for c in b])
                else:
                    return([hex(b) for b in tmp])
        else:
            return res[:]

    def write(self, byte):
        """
        Write byte to serial port.

        Parameters
        ==========
            byteseq - bytearray:
                Sequence of bytes to be written to serial device port
        """
        try:
            # attempt to format
            if isinstance(byte, int):
                byte = byte.to_bytes(1, byteorder="big") # format
            elif isinstance(byte, str):
                byte = ord(byte).to_bytes(1, byteorder="big")
                print(byte)
        except Exception as e:
            print(e)
            return 0
        
        try:
            # Send byte
            res = self.__connection.write(byte)
            self.timeout()
            return res
        except Exception as e:
            self.__stderr(IOError("Could not write to serial port {}.\n".format(self.__port)))
            # raise exceptions.SerialWriteError(mode)
            raise exceptions.SerialWriteError("{:08b}".format(byte))


    def timeout(self, duration=None):
        """
        Set Timeout

        Parameters
        ==========
            duration - (float | int):
                Optional. Timeout duration.
                Default: Instance timeout duration setting
        """
        duration = float(duration or self.__timeout_duration)
        select.select([], [self.__connection], [], duration)

    ###
    ## Helper Methods
    ###

    def __stdout(self, fmt, *args):
        """
        If "quiet" is not set, print output
        """
        if self._is_verbose:
            new_args = []
            for arg in args:
                if isinstance(arg, bytes):
                    arg = int.from_bytes(arg, byteorder="big")
                new_args.append(arg)
            sys.stdout.write(fmt.format(*new_args))

    def __stderr(self, fmt, *args):
        """
        If "quiet" is not set, print output
        """
        if self._is_verbose:
            for arg in args:
                sys.stderr.write(arg)

    def get_port(self):
        """
        Get the device port of current connection.
        """
        self.__stdout("Current Port: {}.\n", self.__port)
        return self.__port

    def get_baud(self):
        """
        Get the baud rate current connection.
        """
        self.__stdout("Current baud: {}.\n", self.__baud)
        return self.__baud

    def get_timeout_duration(self):
        """
        Get the timeout_duration of current connection.
        """
        self.__stdout("Current timeout duration: {}.\n", self.__timeout_duration)
        return self.__timeout_duration
