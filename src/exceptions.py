#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Custom Exceptions for BusPyr

    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>
"""

import utils

class CommandNotFound(Exception):
    def __init__(self, user_input, cmd_type=None):
        self.message = Utils.print_cmd_mode_help(user_input, cmd_type)

class ModeNotFound(Exception):
    def __init__(self, user_input):
        self.message = Utils.print_cmd_mode_help(user_input, 'MODES')

class PrescaleSettingNotFound(Exception):
    def __init__(self):
        self.message = (
            "Error setting prescaler resolution.\n"
            "Available Options:\n"
            "\t0: 1 bit\n\t1: 8 bits\n\t2: 64 bits\n\t3: 256 bits"
        )

class SerialWriteError(Exception):
    def __init__(self, user_input):
        self.message = (
            "Error writing to device.\n"
            "Attempted to write:{}".format(user_input)
        )

class SerialReadError(Exception):
    def __init__(self, length=None):

        self.message = (
            "Error reading all {} bytes from device.\n"
        ) if length != None else (
            "Error reading from device.\n"
        )

class ConnectionError(Exception):
    def __init__(self):
        self.message = (
            "Could not connect to device over serial.\n"
        )
