#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Python package for interfacing with the [BusPirate 3.6](http://dangerousprototypes.com/docs/Bus_Pirate).
    
    A personal, purely educational replacement for [pybuspiratelite](https://github.com/DangerousPrototypes/Bus_Pirate/tree/master/scripts/pyBusPirateLite) also using [pyserial](http://pythonhosted.org/pyserial/).

    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>

"""

from serialconnector import SerialConnector
import exceptions
from commands import COMMANDS, CODES, MASKS, MASK_NAMES
import constants
import utils

class Buspyr(SerialConnector):
    def __init__(self, port=None, baud=constants.BAUD, timeout=0.05, connect=True, is_verbose=False):
        """
        Connect and control Bus Pirate 3b
            FIRMWARE 5.10
            BOOTLOADER 4.4

        Parameters
        ==========
            port - (str):
                Optional. Device descriptor path (com port not tested).
                Default: see `constants.py`
            baud - (int):
                Optional. Serial baud rate.
                Default: see `constants.py`
            timeout - (float):
                Optional. Milliseconds to wait during timeout.
                Default: 0.05
            connect - (bool)
                Optional. Whether or not to connect on class initialization.
                Default: True
            is_verbose - (bool)
                Optional. Stream output to `stdout`
                Default: False
                

        See [BitBang](http://dangerousprototypes.com/docs/Bitbang) Documentation
        """
        super().__init__(port, baud, timeout, is_verbose)

        self.__stdout = self._SerialConnector__stdout
        self.__stderr = self._SerialConnector__stderr

        self.__io = 0b01000000
        self.__states = 0b10000000

        # State flag
        self.__is_on = False

        if connect:
            self.connect()

    def connect(self):
        self.__connection = super().connect()
        self.reset()
        self.enter_bitbang_mode()

    def reset(self):
        """
        Reset Buspirate Connection
        """
        self.__connection.flush()
        self.execute("RESET")
        return self

    def send_command(self, cmd):
        """
        Send a command via a command string.

        Parameters
        ==========
            cmd - str:
                Command string to lookup as bytestring and send to device
        """
        try:
            byte = self.get_command_byte(cmd.upper())
        except Exception as e:
            self.__stderr(e)
            raise e

        if byte == None:
          self.__stderr("Command '{}' not found\n".format(cmd))
          raise exceptions.CommandNotFound(cmd)

        return self.send_byte(byte)


    def send_byte(self, byte):
        """
        Execute a byte

        Parameters
        ==========
            byte - str | bytearray:
                Byte to send to device.

        """

        return self.write(byte)


    def execute(self, cmd):
        """ Execute a command """
        if isinstance(cmd, str):
            return self.send_command(cmd)
        else:
            return self.send_byte(cmd)

    def set_mode(self, mode):
        """
        Set active mode.

        TODO: Allow this method to accept str, byte and int values

        Parameters
        ==========
            mode - str:
                String of mode to use.
        """
        try:
            self.write(self.get_command_byte(mode))
            self.current_mode = mode
        except (AttributeError, KeyError) as e:
            self.__stderr(exceptions.ModeNotFound(mode))
        except Exception as e:
            self.__stderr(e)
            raise e
            # raise e("There was an unknown error.\n")
        finally:
            self.__stdout("Mode set to {}\n", mode)
            return self


    ###
    ##  Action Methods
    ###

    def enter_bitbang_mode(self, reset=False):
        """
        Connect to BusPirate in bitbang mode
    
        From (Bitbang Docs)[http://dangerousprototypes.com/docs/Bitbang]:
        'One way to ensure that you're at the command line 
        is to send <enter> at least 10 times, and then send '#' to reset. 
        
        Next, send 0x00 to the command line 20+ times 
        until you get the BBIOx version string.'

        """

        success_flag = False

        if reset:
            self.reset()

        self.__connection.flush()

        self.__stdout("Entering bitbang mode...")

        for i in range(constants.MAX_CONNENCTION_ATTEMPTS):
            res = self.execute("BBIO")

            output = self.read(5)
            self.__stdout(".")

            if b"BBIO1" in output:
                self.__connection.flush()
                self.__stdout(" Success!\n")
                return self

        self.__stdout("Could not enter bitbang mode.")
        self.__stdout('\n')

        raise exceptions.ConnectionError()


    ###
    ## Pin Configuration Methods
    ###

    def power_toggle(self):
        """
        Toggle power to pins.

        Wrapper for toggling `self.power_on()` and `self.power_off()`
        """
        return self.power_off() if self.__is_on else self.power_on()

    def power_on(self):
        """
            Turn on power to pins.
        """
        if not self.__is_on:
            self.set_state(MASKS.get('PWR'), is_high=True)
        self.__is_on = True
        return self

    def power_off(self):
        """
        Turn off power to pins.
        """
        if self.__is_on:
            self.set_state(MASKS.get('PWR'), is_high=False)
        self.__is_on = False
        return self

    def set_pwm(self, period, duty=0.5, prescaler=0):
        """
        Configure PWM (pulse-wave modulation) settings.

        From [PIC24F datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/39706a.pdf):
            Specify the PWM period by writing to PRy, the TMRy Period register.
            The PWM period can be calculated using the following formula:

                PWM Period = [(PRy) + 1] • TCY • (TMRy Prescale Value)
                PWM Frequency = 1/[PWM Period]

                Note 1: Based on TCY = 2/FOSC

        Additional notes from [PWM Setup Script](http://codepad.org/qtYpZmIF)

        Parameters
        ==========
            period: int
                Pulse period in milliseconds
            duty: int|float
                Duty cycle. Scaled [0,1] (0 to 1, inclusive)
                Default: 0.5 (50%)
            prescaler: int
                Prescaler resolution. Available options:
                    0: 1 bit
                    1: 8 bits
                    2: 64 bits
                    3: 256 bits
                Default: 0 (1 bit)
        """
        try:
            prescaler = constants.PWM_PRESCALER_OPTS[int(prescaler)]
        except IndexError as e:
            self.__stderr(e)
            raise exceptions.PrescaleSettingNotFound(e)
        else:
            TCY = 2.0 / constants.PWM_OSC_FREQUENCY
            PRy = (int(period) / (TCY * prescaler)) - 1
            OCR = PRy * duty

            self.write(bytearray([
                int(prescaler),
                (int(OCR) >> 8) & b"\xFF",
                (int(OCR)) & b"\xFF",
                (int(PRy) >> 8) & b"\xFF",
                (int(PRy)) & b"\xFF",
            ]))

    ###
    ## Helper Methods
    ###

    def get_cache(self):
        """
        Return latest cache of the `read` method.
        """
        return self._cache


    def clear_cache(self):
        """
        Clear the cache.
        """
        self.__stdout("Clearing Cache.\n")
        self._cache = ""

    def get_cmd_str_map(self, cmd_str=None):
        if cmd_str and cmd_str in COMMANDS:
            res = COMMANDS.get(cmd_str)
            return res
        else:
            return COMMANDS

    def get_cmd_code_map(self, cmd_code=None):
        cmd_code = str(cmd_code)
        if cmd_code and cmd_code in CODES:
            return CODES.get(cmd_code)
        else:
            return CODES

    def get_command_byte(self, cmd_name, cmd_type=None):
        """
        Format and Validate key reference to command byte.

        Parameters
        ==========
        cmd_name - (str):
          String representation of command
        """
      
        if cmd_type:
            cmd_name = '{:s}_{:s}'.format(cmd_type, cmd_name)

        try:
            return COMMANDS.get(cmd_name.upper())[0]
            cmd_byte
        except AttributeError as e:
            print("Attribute Exception:", e)
        except Exception as e:
            print("General Exception:", e)

        raise exceptions.CommandNotFound(cmd_name, cmd_type)


    def get_mask_as_list(self, mask):
        try:
            mask = list(format(mask, '08b'))
            return mask
        except Exception as e:
            self.__stderr(e)

    def get_mask_name(self, mask):
        index = self.get_mask_as_list(mask).index('1')
        if index <= len(MASK_NAMES):
          return MASK_NAMES[index - 1]

    def set_io(self, mask, is_input=True):

        if is_input:
            self.__io |= mask
        else:
            self.__io &= ~mask

        if self._is_verbose:
            # redundancy to avoid calculating if not verbose
            name = self.get_mask_name(mask)
            io = 'IN' if is_input else 'OUT'
            self.__stdout("{} I/O is {}.\n", name, io)
      
        return self.execute(self.__io)


    def get_io(self):
        return self.__io

    def set_state(self, mask, is_high=False):

        if is_high:
            self.__states |= mask
        else:
            self.__states &= ~mask

        if self._is_verbose:
            # redundancy to avoid calculating if not verbose
            name = self.get_mask_name(mask)
            state = 'ON' if is_high else 'OFF'
            self.__stdout("{:s} state is {:s}.\n", name, state)

        return self.execute(self.__states)

    def get_states(self, name=None, as_dict=False):
        if as_dict:
            mask_map = {MASK_NAMES[i]: state 
            for i, state in enumerate(self.get_mask_as_list(self.__states)) 
            if i < len(MASK_NAMES)}

            return mask_map.get(name) if name else mask_map
        else:
            return self.__states

