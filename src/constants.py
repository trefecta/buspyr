#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Bus Pirate 3.6 Constants and Settings
    See [Documentation](http://dangerousprototypes.com/docs/Bitbang)

    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>
"""

VENDOR_ID = 0x0403
PRODUCT_ID = 0x6001

BAUD = 115200

MAX_CONNENCTION_ATTEMPTS = 25

PWM_PRESCALER_OPTS = [
    1,
    8,
    64,
    256
]

PWM_OSC_FREQUENCY = 32000000 # 32MHz crystal oscillator

## Hex codes for selecting modes and protocols
BBIO       = 0x00
SPI        = 0x01
I2C        = 0x02
UART       = 0x03
ONEWIRE    = 0x04
RAW        = 0x05
JTAG       = 0x06

HASH       = 0x23 # literal '#'
ENTER      = 0x0D # Carriage Return
RESET      = 0x0F
SELF_TEST  = 0x10

PWM        = 0x12
PWM_CLEAR  = 0x13

ADC        = 0x14
ADC_CONT   = 0x15

AUX        = 0x16

CS_MASK    = 0x01
MISO_MASK  = 0x02
CLK_MASK   = 0x04
MOSI_MASK  = 0x08
AUX_MASK   = 0x10
VPU_MASK   = 0x20
PWR_MASK   = 0x40

CLEAR      = 0x80
