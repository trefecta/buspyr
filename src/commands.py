#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    [All BusPirate 3.6 commands](https://docs.google.com/spreadsheets/d/1RTwl1QopAR6DFUyJT0gwPgRnqhLqfd37TvezzquqlxM)

    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>
"""
from collections import OrderedDict

from constants import (
    PWR_MASK,
    VPU_MASK,
    AUX_MASK,
    MOSI_MASK,
    CLK_MASK,
    MISO_MASK,
    CS_MASK,
    BBIO,
    SPI,
    I2C,
    UART,
    ONEWIRE,
    RAW,
    JTAG,
    ENTER,
    HASH,
    RESET,
    SELF_TEST,
    PWM,
    PWM_CLEAR,
    ADC,
    ADC_CONT,
    AUX,
    CLEAR
)

import constants



COMMAND_TYPES = {
    'BBIO':         (BBIO,      'Enter bitbang mode.'),

    'SPI':          (SPI,       'Enter binary SPI mode.'),
    'I2C':          (I2C,       'Enter binary I2C mode.'),
    'UART':         (UART,      'Enter binary UART mode.'),
    'ONEWIRE':      (ONEWIRE,   'Enter binary 1-Wire mode (Same as `1WIRE`).'),
    'RAW':          (RAW,       'Enter binary raw-wire mode (2wire, 3wire support).'),
    'JTAG':         (JTAG,      'Enter OpenOCD JTAG mode.'),

    'ENTER':        (ENTER,     'Key command: <enter> / carriage return.'),
    'HASH':         (HASH,      'Key command: literal `#`.'),
    'RESET':        (RESET,     'Reset Bus Pirate.'),
    'SELF_TEST':    (SELF_TEST,     'Run Bus Pirate self-tests.'),

    'AUX':          (AUX,       'Measure frequency on AUX pin.'),

    'PWM':          (PWM,       'Setup PWM (requires 5 additional configuration bytes).'),
    'PWM_CLEAR':    (PWM_CLEAR, 'Clear PWM.'),
    'PWM_CLR':      (PWM_CLEAR, 'Clear PWM (Same as `PWM_CLEAR`).'),

    'ADC':          (ADC,       'Take instantaneous voltage probe (returns 2 bytes).'),
    'ADC_CONT':     (ADC_CONT,  'Take continuous voltage probe (returns 2 bytes).'),

    'PWR_MASK':     (PWR_MASK,    'POWER state mask.'),
    'VPU_MASK':     (VPU_MASK,    'VPU state mask.'),
    'AUX_MASK':     (AUX_MASK,    'AUX state mask.'),
    'MOSI_MASK':    (MOSI_MASK,   'MOSI state mask.'),
    'CLK_MASK':     (CLK_MASK,    'CLK state mask.'),
    'MISO_MASK':    (MISO_MASK,   'MISO state mask.'),
    'CS_MASK':      (CS_MASK,     'CS state mask.'),

    'CLEAR':        (CLEAR,     'Clear all pins.')
}

MASKS = OrderedDict({
    'PWR': PWR_MASK,
    'VPU': VPU_MASK,
    'AUX': AUX_MASK,
    'MOSI': MOSI_MASK,
    'CLK': CLK_MASK,
    'MISO': MISO_MASK,
    'CS': CS_MASK
}) # big endian

MASK_NAMES = list(MASKS.keys()) # big endian



# Concatenate command names into `{GROUP}_{COMMAND}
# Unless command does not have a group (`RESET`, `SELF_TEST`, `AUX`)
COMMANDS = {}
CODES = {}
for cmd_type, cmds in COMMAND_TYPES.items():
    if isinstance(cmds, dict):
        for k,v in cmds.items():
            cmd_str = '{}_{}'.format(cmd_type, k)
            COMMANDS[cmd_str] = v
            CODES[str(v[0])] = cmd_str
    else:
        COMMANDS[cmd_type] = cmds
        CODES[str(cmds[0])] = cmd_type

