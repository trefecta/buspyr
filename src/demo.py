#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Demonstration of Buspyr Bitbang functionality.
"""

from buspyr import Buspyr
from commands import MASKS as masks

def cli():
    with Buspyr(is_verbose=True) as bp:
        print(bp.read(to_hex=False))
        bp.power_on()
        
        for mask in ['AUX', 'CS']:
            bp.set_io(masks.get(mask), is_input=False)
            bp.set_state(masks.get(mask), is_high=True)
            print(bp.read(to_hex=True))

        for mask in ['AUX', 'CS']:
            input("Press <ANY> key to turn {} off.".format(mask))
            bp.set_state(masks.get(mask), is_high=False)
            print(bp.read(to_hex=True))

        input("Press <ANY> key to turn PWR off.")
        bp.execute(0)
        print(bp.read(to_hex=True))

        input()
        bp.power_off()

if __name__ == '__main__':
    cli()