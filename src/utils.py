#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Buspyr utility functions
    
    See [Documentation](http://dangerousprototypes.com/docs/Bitbang)
    :copyright: (c) 2018 by Tre Tomaszewski <tre.tomaszewski@gmail.com>
"""
from commands import COMMANDS

# Helper functions

def print_options_message(cmd_type=None):
    """ Return a specific command and the hex code """
    tmpl = "\t{:<8s} : {:02X} : {:<64}"
    try:
        return '\n'.join([
          tmpl.format(k, int(v[0].hex(), 16), v[1]) 
          for k, v in COMMANDS.items()
        ])
    except Exception as e:
        print(e)

def print_cmd_mode_help(user_input=None, cmd_type=None):
    """ Return dialog containing all commands. """
    return '{:s} is not a {:s}.\nAvailable Commands:\n{:s}'.format(
        str(user_input) or 'Selected item',
        str(cmd_type).upper() or 'command' ,
        print_options_message(cmd_type)
    )
