# Buspyr
Python package for interfacing with the [BusPirate 3.6](http://dangerousprototypes.com/docs/Bus_Pirate).

Built as a personal, purely educational replacement for [pybuspiratelite](https://github.com/DangerousPrototypes/Bus_Pirate/tree/master/scripts/pyBusPirateLite) also using [pyserial](http://pythonhosted.org/pyserial/).
